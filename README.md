__**Factorio Server Details**__ 
Server is Public and should show up in the server listing.

**Game Version:** Standard 0.16.36 (Alpha)

**To get this version:**
On Steam - Right click Factorio and select Properties > Click Betas > Select NONE 
Direct Download - <https://factorio.com/download> (You will need to login).


**Server Name:** Simon's Amazeballs Server

**Server Address:** factorio.simonroth.net - Server is also listed in Public Listing.

**Password:** See Discord - https://discord.gg/PqK6Qap


**Mods Download:** Download this repository via ZIP and save locally. You will then unzip to your mods folder, see below.

Mod zip contains each mod in it's zip folder. IMPORTANT: Leave the individual mods as zip files!!! Unzip to your Install Directory "mod" folder.

**Direct Install Mods:**
```
Navigate to your install dicretory then the Mods folder - i.e. G:\Secondary Apps\Factorio\mods
Extract the ZIP file contents to the mods directory - Do NOT Unpack the individual mods!
Launch the game and select Mods, verify they are appearing and enabled.
```

**Steam Mod Install:**
```
Navigate to C:\Users\<username>\AppData\Roaming\Factorio\mods (replace <username> with your username)
Extract the ZIP file contents to the mods directory - Do NOT Unpack the individual mods!
Launch the game and select Mods, verify they are appearing and enabled.
```

**Mod List:**
```
Current Mod List for Simon's Amazeballs Server.
Last Updated: Feb. 8th, 2018.

Auto Fill - v2.0.1
Air Filtering Advanced - v0.6.15
Bob's Assembling Machines - v0.16.1
Bob's Functions Library - v0.16.5
Bob's Mining - v0.16.0
Bob's Power - v0.16.4
Bob's Revamp - v0.16.2
Bob's Tech - v0.16.5
Bob's Vehicle Equipment - v0.16.2
Bob's Warfare - 0.16.6
Bottleneck - v0.9.1
Charcoal Burner - v1.16.18
CNCs Sulfur Mod - v1.0.4
Crafting Speed Research - v0.2.0
Endless Resource with Full Yield - v0.16
Flow Control - v3.0.3
Icon Size Fix - v0.0.4
Power And Armor - v1.16.30
Side Inserters - v2.3.4
Simon's Lights - v0.0.1
Skan Advanced Solar - v1.1.0
Squeak Through - v1.2.2
TinyStart - v0.16.6
YARM Resource Monitor - v0.7.304
```